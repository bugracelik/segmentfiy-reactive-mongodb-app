package com.segmentify.reactive.mongodbapp.api;

import com.segmentify.reactive.mongodbapp.entity.Employee;
import com.segmentify.reactive.mongodbapp.repo.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeRepository employeeRepository;

    @GetMapping("/{id}")
    public Mono<Employee> getById(@PathVariable String id) {
        return employeeRepository.findById(id);
    }

    @GetMapping()
    public Flux<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @GetMapping("/name/{name}")
    public Mono<Employee> getByName(@PathVariable String name) {
        return employeeRepository.findByName(name);
    }

    @PutMapping("save")
    public Mono<Employee> save(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @GetMapping("id/{id}")
    public Mono<Boolean> existById(@PathVariable String id) {
        return employeeRepository.existsById(id);
    }

    @DeleteMapping("id/{id}")
    public Mono<Void> deleteById(@PathVariable String id){
        return employeeRepository.deleteById(id);
    }

}
