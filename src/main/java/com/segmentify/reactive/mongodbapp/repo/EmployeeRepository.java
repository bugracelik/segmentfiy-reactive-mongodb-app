package com.segmentify.reactive.mongodbapp.repo;

import com.segmentify.reactive.mongodbapp.entity.Employee;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface EmployeeRepository extends ReactiveMongoRepository<Employee, String> {
    Mono<Employee> findByName(String s);
}
